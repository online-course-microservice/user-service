var express = require('express');
var router = express.Router();

const usersRouter = require('./users');
const refreshTokensRouter = require('./refreshTokens');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({ status: 'success', message: 'welcome to service users' });
});

router.use('/users', usersRouter);
router.use('/refresh_tokens', refreshTokensRouter);

module.exports = router;
